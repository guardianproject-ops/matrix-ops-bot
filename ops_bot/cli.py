import secrets
import sys
from typing import Any

import click

from ops_bot.config import RoutingKey, hook_types, load_config, save_config


@click.group()
@click.option(
    "--config-file", help="the path to the config file", default="config.json"
)
@click.pass_context
def cli(ctx: Any, config_file: str) -> None:
    ctx.obj = config_file
    pass


@cli.command(help="Add a new routing key to the configuration file")
@click.option(
    "--name", help="a friendly detailed name for the hook so you can remember it later"
)
@click.option(
    "--hook-type",
    help="The type of webhook to add",
    type=click.Choice(["gitlab", "pagerduty", "aws-sns"], case_sensitive=False),
)
@click.option("--room-id", help="The room ID to send the messages to")
@click.pass_obj
def add_hook(config_file: str, name: str, maybe_hook_type: str, room_id: str) -> None:
    settings = load_config(config_file)
    path_key = secrets.token_urlsafe(30)
    secret_token = secrets.token_urlsafe(30)
    if name in set([key.name for key in settings.routing_keys]):
        print("Error: A hook with that name already exists")
        sys.exit(1)
    if maybe_hook_type not in hook_types:
        print(f"Invalid hook type {maybe_hook_type}")
        print("Must be one of ", ", ".join(hook_types))
    settings.routing_keys.append(
        RoutingKey(
            name=name,
            path_key=path_key,
            secret_token=secret_token,
            room_id=room_id,
            hook_type=maybe_hook_type,  # type: ignore[arg-type]
        )
    )
    save_config(settings)

    url = f"/hook/{path_key}"

    print("Hook added successfully")
    print()
    print("Your webhook URL is:")
    print(f"\t{url}")
    print("The secret token is:")
    print(f"\t{secret_token}")
