COLOR_OK = "#33cc33"  # green
COLOR_ALARM = "#dc3545"  # red
COLOR_INFO = "#17a2b8"  # blue
COLOR_WARNING = "#ffc107"  # orange
COLOR_UNKNOWN = "#ffa500"  # yellowish
