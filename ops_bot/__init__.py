from pathlib import Path

__version__ = "0.1.0"


def get_project_root() -> Path:
    return Path(__file__).parent
