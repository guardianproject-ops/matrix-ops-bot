import json
from typing import Any, List, Tuple

from fastapi import Request

from ops_bot.common import COLOR_ALARM, COLOR_UNKNOWN
from ops_bot.config import RoutingKey


def urgency_color(urgency: str) -> str:
    if urgency == "high":
        return COLOR_ALARM
    else:
        return COLOR_UNKNOWN


async def parse_pagerduty_event(
    route: RoutingKey,
    payload: Any,
    request: Request,
) -> List[Tuple[str, str]]:
    """
    Parses a pagerduty webhook v3 event into a human readable message.
    Returns a tuple where the first item is plain text, and the second item is matrix html formatted text
    """
    event = payload["event"]
    # evt_id = event["id"]
    # event_type = event["event_type"]
    # resource_type = event["resource_type"]
    # occurred_at = event["occurred_at"]
    data = event["data"]
    data_type = data["type"]

    if data_type == "incident":
        url = data["html_url"]
        status: str = data["status"]
        title: str = data["title"]
        service_name: str = data["service"]["summary"]
        urgency: str = data.get("urgency", "high")
        plain = f"{status}: on {service_name}:  {title} {url}"
        header_str = f"{status.upper()}[{urgency}]"
        if status == "resolved":
            color = "#33cc33"  # green
        else:
            color = urgency_color(urgency)
        formatted = f"<strong><font color={color}>{header_str}</font></strong> on {service_name}:  [{title}]({url})"
        return [(plain, formatted)]

    payload_str = json.dumps(payload, sort_keys=True, indent=2)
    return [
        (
            "unhandled",
            f"""**unhandled pager duty event** (this may or may not be a critical problem, please look carefully)
    <pre><code class="language-json">{payload_str}</code></pre>
    """,
        )
    ]
