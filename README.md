# matrix-ops-bot

> a bot for ops in matrix

This bot catches webhooks and forwards them as messages to matrix rooms.

Current supported webhooks:

* PagerDuty
* AWS SNS
* Gitlab
* Prometheus Alertmanager

## Usage

See [config.json.sample](config.json.sample) for a sample config file.

Once you have a basic config (leave the routing_keys an empty list), you can easily add new webhooks with

```console
$ poetry run config add-hook --name my-hook-name --hook-type gitlab --room-id '!abcd1234:matrix.org'

Hook added successfully

Your webhook URL is:
        /hook/vLyPN5mqXnIGE-4o9IKJ3vsOMU1xYEKBW8r4WMvP
The secret token is:
        6neuYcncR2zaeQiEoawXdu6a4olsfH447tFetfvv
```

Note: Register your bot user manually. This program does not register a new
user. You must also accept invitations to join the room automatically.


```
docker run --rm  \
--name ops-bot \
--volume /path/to/data:/data:rw \
--volume /path/to/config:/config:ro \
registry.gitlab.com/guardianproject-ops/matrix-ops-bot:main
```

or docker-compose:

```yaml
version: "3.8"
services:
  bot:
    image: registry.gitlab.com/guardianproject-ops/matrix-ops-bot:main
    user: "1000"
    ports: 
      - "0.0.0.0:1111:1111"
    volumes:
      - /home/admin/bot/data:/data:rw
      - /home/admin/bot/config:/config:ro
    logging:
      driver: "json-file"
      options:
        max-size: "100m"
        max-file: "3"
```

## Dev Setup

```
poetry install
poetry run start
```

## License

```
matrix-ops-bot
Copyright (C) 2022 Abel Luck <abel@guardianproject.info>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```


This project uses pieces of [maubot/gitlab](https://github.com/maubot/gitlab),
which is also AGPL. These files have been noted with a comment header.
