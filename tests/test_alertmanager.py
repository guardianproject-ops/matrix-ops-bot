import json

from ops_bot.alertmanager import prometheus_alert_to_markdown

payload = json.loads(
    """
{
  "receiver": "matrix",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "InstanceDown",
        "environment": "monitoring.example.com",
        "instance": "webserver.example.com",
        "job": "node_exporter",
        "severity": "critical"
      },
      "annotations": {
        "description": "webserver.example.com of job node_exporter has been down for more than 5 minutes.",
        "summary": "THIS IS A TEST Instance webserver.example.com down"
      },
      "startsAt": "2022-06-23T11:53:14.318Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://monitoring.example.com:9090",
      "fingerprint": "9cd7837114d58797"
    }
  ],
  "groupLabels": {
    "alertname": "InstanceDown"
  },
  "commonLabels": {
    "alertname": "InstanceDown",
    "environment": "monitoring.example.com",
    "instance": "webserver.example.com",
    "job": "node_exporter",
    "severity": "critical"
  },
  "commonAnnotations": {
    "description": "webserver.example.com of job node_exporter has been down for more than 5 minutes.",
    "summary": "Instance webserver.example.com down"
  },
  "externalURL": "https://alert.example",
  "version": "4",
  "groupKey": "",
  "truncatedAlerts": 0
}"""
)


def test_alertmanager():
    r = prometheus_alert_to_markdown(payload)
    assert len(r) == 1
    plain, formatted = r[0]

    print(formatted)

    assert (
        "FIRING" in plain
        and "Instance" in plain
        and "webserver.example.com" in plain
        and "THIS IS A TEST" in plain
        and "critical" in plain
    )
    assert (
        "FIRING" in formatted
        and "Instance" in formatted
        and "webserver.example.com" in formatted
        and "Instance webserver.example.com down" in formatted
        and "critical" in formatted
        and "https://alert.example/#/silences/new?filter={alertname%3D%22InstanceDown%22}"
        in formatted
    )
