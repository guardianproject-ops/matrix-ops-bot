import json

from ops_bot import __version__, aws


def test_version() -> None:
    assert __version__ == "0.1.0"


sns_subscribtion_unsubscribe = """{
  "Type" : "UnsubscribeConfirmation",
  "MessageId" : "47138184-6831-46b8-8f7c-afc488602d7d",
  "Token" : "2336412f37...",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Message" : "You have chosen to deactivate subscription arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55.\\nTo cancel this operation and restore the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37fb6...",
  "Timestamp" : "2012-04-26T20:06:41.581Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEHXgJm...",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
}"""
sns_subscribtion_confirm = """{
  "Type" : "SubscriptionConfirmation",
  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
  "Token" : "2336412f37...",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\\nTo confirm the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37...",
  "Timestamp" : "2012-04-26T20:45:04.751Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
}"""

sns_notification = """{
  "Type" : "Notification",
  "MessageId" : "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Subject" : "My First Message",
  "Message" : "Hello world!",
  "Timestamp" : "2012-05-02T00:54:06.655Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEw6JRN...",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96"
}"""

sns_signin = """
{
  "version": "0",
  "id": "000000-e441-44ce-22c1-00000000",
  "detail-type": "AWS Console Sign In via CloudTrail",
  "source": "aws.signin",
  "account": "1234567890",
  "time": "2025-01-31T14:03:15Z",
  "region": "eu-north-1",
  "resources": [],
  "detail": {
    "eventVersion": "1.09",
    "userIdentity": {
      "type": "IAMUser",
      "principalId": "ABCDEFGHIJKLMNOPQRSTU",
      "arn": "arn:aws:iam::1234567890:user/user@example.com",
      "accountId": "1234567890",
      "userName": "user@example.com"
    },
    "eventTime": "2025-01-31T14:03:15Z",
    "eventSource": "signin.amazonaws.com",
    "eventName": "ConsoleLogin",
    "awsRegion": "eu-north-1",
    "sourceIPAddress": "193.0.0.0.1",
    "userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:134.0) Gecko/20100101 Firefox/134.0",
    "requestParameters": null,
    "responseElements": {
      "ConsoleLogin": "Success"
    },
    "additionalEventData": {
      "LoginTo": "https://console.aws.amazon.com/console/home",
      "MobileVersion": "No",
      "MFAIdentifier": "arn:aws:iam::1234567890:u2f/user/user@example.com/user-omg-my-yubikey",
      "MFAUsed": "Yes"
    },
    "eventID": "000000-1539-4d7f-b6cc-000000000",
    "readOnly": false,
    "eventType": "AwsConsoleSignIn",
    "managementEvent": true,
    "recipientAccountId": "1234567890",
    "eventCategory": "Management",
    "tlsDetails": {
      "tlsVersion": "TLSv1.3",
      "cipherSuite": "TLS_AES_128_GCM_SHA256",
      "clientProvidedHostHeader": "eu-north-1.signin.aws.amazon.com"
    }
  }
}
"""

sns_signin_failure = """
{
  "version": "0",
  "id": "0000-a6cf-b920-6e14-000000",
  "detail-type": "AWS Console Sign In via CloudTrail",
  "source": "aws.signin",
  "account": "1234567890",
  "time": "2025-01-31T14:01:49Z",
  "region": "eu-north-1",
  "resources": [],
  "detail": {
    "eventVersion": "1.09",
    "userIdentity": {
      "type": "IAMUser",
      "principalId": "AIDARWPFIVFS76W7ZBVBO",
      "accountId": "1234567890",
      "accessKeyId": "",
      "userName": "user@example.com"
    },
    "eventTime": "2025-01-31T14:01:49Z",
    "eventSource": "signin.amazonaws.com",
    "eventName": "ConsoleLogin",
    "awsRegion": "eu-north-1",
    "sourceIPAddress": "193.0.0.0.1",
    "userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:134.0) Gecko/20100101 Firefox/134.0",
    "errorMessage": "Failed authentication",
    "requestParameters": null,
    "responseElements": {
      "ConsoleLogin": "Failure"
    },
    "additionalEventData": {
      "LoginTo": "https://console.aws.amazon.com/console/home?",
      "MobileVersion": "No",
      "MFAUsed": "Yes"
    },
    "eventID": "00000-572b-4006-8d9f-00000",
    "readOnly": false,
    "eventType": "AwsConsoleSignIn",
    "managementEvent": true,
    "recipientAccountId": "1234567890",
    "eventCategory": "Management",
    "tlsDetails": {
      "tlsVersion": "TLSv1.3",
      "cipherSuite": "TLS_AES_128_GCM_SHA256",
      "clientProvidedHostHeader": "eu-north-1.signin.aws.amazon.com"
    }
  }
}
"""

sns_sts_saml = """
{
  "version": "0",
  "id": "f7ea4d10-ee27-ee26-efa3-7fe107e12ba4",
  "detail-type": "AWS API Call via CloudTrail",
  "source": "aws.sts",
  "account": "1234567890",
  "time": "2025-01-31T15:34:20Z",
  "region": "eu-west-1",
  "resources": [],
  "detail": {
    "eventVersion": "1.08",
    "userIdentity": {
      "type": "SAMLUser",
      "principalId": "redacted:user@example.com",
      "userName": "user@example.com",
      "identityProvider": "redacted"
    },
    "eventTime": "2025-01-31T15:34:20Z",
    "eventSource": "sts.amazonaws.com",
    "eventName": "AssumeRoleWithSAML",
    "awsRegion": "eu-west-1",
    "sourceIPAddress": "54.0.0.0",
    "userAgent": "aws-internal/3 aws-sdk-java/1.12.779 Linux/4.14.355-275.570.amzn2.x86_64 OpenJDK_64-Bit_Server_VM/17.0.13+11-LTS java/17.0.13 vendor/Amazon.com_Inc. cfg/retry-mode/standard cfg/auth-source#imds",
    "requestParameters": {
      "sAMLAssertionID": "_d1e7a65e-2298-4c0f-88b7-4e62f5a7a00c",
      "roleSessionName": "user@example.com",
      "roleArn": "arn:aws:iam::1234567890:role/aws-reserved/sso.amazonaws.com/eu-west-1/AWSReservedSSO_AWSAdministratorAccess_abcd1234",
      "principalArn": "arn:aws:iam::1234567890:saml-provider/AWSSSO_aslksdafkj212_DO_NOT_DELETE",
      "durationSeconds": 3600
    },
    "responseElements": {
      "credentials": {
        "accessKeyId": "ASredacted",
        "sessionToken": "redacted",
        "expiration": "Jan 31, 2025, 4:34:19 PM"
      },
      "assumedRoleUser": {
        "assumedRoleId": "redacted:user@example.com",
        "arn": "arn:aws:sts::1234567890:assumed-role/AWSReservedSSO_AWSAdministratorAccess_abcd1234/user@example.com"
      },
      "subject": "user@example.com",
      "subjectType": "persistent",
      "issuer": "https://portal.sso.eu-west-1.amazonaws.com/saml/assertion/NjQ2NzQ5Mjg5MzMxX2lucy1hY2MwZGYyZTYzOGZkYjNm",
      "audience": "https://signin.aws.amazon.com/saml",
      "nameQualifier": "12346"
    },
    "requestID": "42ba0a94-8f19-4a1b-9bac-9e8eb6ac1c15",
    "eventID": "0f652a57-36ad-4a85-8651-252f256660e8",
    "readOnly": true,
    "resources": [
      {
        "accountId": "1234567890",
        "type": "AWS::IAM::Role",
        "ARN": "arn:aws:iam::1234567890:role/aws-reserved/sso.amazonaws.com/eu-west-1/AWSReservedSSO_AWSAdministratorAccess_abcd1234"
      },
      {
        "accountId": "1234567890",
        "type": "AWS::IAM::SAMLProvider",
        "ARN": "arn:aws:iam::1234567890:saml-provider/AWSSSO_aslksdafkj212_DO_NOT_DELETE"
      }
    ],
    "eventType": "AwsApiCall",
    "managementEvent": true,
    "recipientAccountId": "1234567890",
    "eventCategory": "Management",
    "tlsDetails": {
      "tlsVersion": "TLSv1.3",
      "cipherSuite": "TLS_AES_128_GCM_SHA256",
      "clientProvidedHostHeader": "sts.eu-west-1.amazonaws.com"
    }
  }
}
"""


def wrap_sns_msg(msg: str) -> dict:
    return {"Type": "Notification", "Message": msg}


async def test_aws_sns_notification() -> None:
    r = await aws.parse_sns_event(None, json.loads(sns_notification), None)
    assert r[0][0] == "My First Message\nHello world!"
    assert (
        r[0][1]
        == "<strong><font color=#dc3545>My First Message</font></strong>\n<p>Hello world!</p>"
    )


async def test_aws_sns_subscribe() -> None:
    r = await aws.parse_sns_event(None, json.loads(sns_subscribtion_confirm), None)
    print(r)
    expected = "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.\n\nhttps://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37..."
    assert r[0] == (expected, expected)


async def test_aws_sns_unsubscribe() -> None:
    r = await aws.parse_sns_event(None, json.loads(sns_subscribtion_unsubscribe), None)
    print(r)
    expected = "You have chosen to deactivate subscription arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55.\nTo cancel this operation and restore the subscription, visit the SubscribeURL included in this message.\n\nhttps://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37fb6..."
    assert r[0] == (expected, expected)


async def test_aws_sns_signin() -> None:
    r = await aws.parse_sns_event(None, wrap_sns_msg(sns_signin), None)
    print(r)
    expected = "<font color=#dc3545>**🚨 ALERT[AwsConsoleSignIn]** </font>: AWS Console Sign detected by user `user@example.com`.<br/>- **Region**: eu-north-1<br/>- **MFA Used**: Yes<br/>- **Event Time**: 2025-01-31T14:03:15Z<br/>- **Account ID**: 1234567890"
    assert r[0][1] == expected


async def test_aws_sns_signin_failure() -> None:
    r = await aws.parse_sns_event(None, wrap_sns_msg(sns_signin_failure), None)
    print(r)
    expected = "<font color=#ffc107>**🚨 ALERT[AwsConsoleSignIn]** </font>: Failed AWS Console Sign attempt by user `user@example.com`.<br/>- **Region**: eu-north-1<br/>- **MFA Used**: Yes<br/>- **Error Message**: Failed authentication<br/>- **Event Time**: 2025-01-31T14:01:49Z<br/>- **Account ID**: 1234567890"
    assert r[0][1] == expected


async def test_aws_sns_sts_saml() -> None:
    r = await aws.parse_sns_event(None, wrap_sns_msg(sns_sts_saml), None)
    print(r)
    expected = "<font color=#dc3545>**🚨 ALERT[AssumeRoleWithSAML]** </font>: AWS SAML Sign detected by user `user@example.com`.<br/>- **Region**: eu-west-1<br/>- **Assumed Role**: arn:aws:sts::1234567890:assumed-role/AWSReservedSSO_AWSAdministratorAccess_abcd1234/user@example.com<br/>- **Event Time**: 2025-01-31T15:34:20Z<br/>- **Account ID**: 1234567890"
    assert r[0][1] == expected
