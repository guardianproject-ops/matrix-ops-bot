ARG PYTHON_VERSION=3.11
FROM docker.io/python:${PYTHON_VERSION}-alpine as builder
ARG LIBOLM_VERSION=3.2.16
RUN apk add --no-cache \
    make \
    cmake \
    gcc \
    g++ \
    git \
    libffi-dev \
    yaml-dev \
    python3-dev

RUN mkdir -p /app/ops_bot
COPY requirements.frozen.txt /app/requirements.txt
RUN pip install --prefix="/python-libs" -r /app/requirements.txt

FROM docker.io/python:${PYTHON_VERSION}-alpine

COPY --from=builder /python-libs /usr/local
COPY --from=builder /usr/local/lib/libolm* /usr/local/lib/

RUN apk add --no-cache \
    libstdc++ bash

RUN mkdir -p /app/ops_bot /app/templates
COPY ops_bot/ /app/ops_bot/
COPY templates/ /app/templates/
ENV MATRIX_STORE_PATH=/data
ENV BOT_CONFIG_FILE=/config/config.json
VOLUME ["/data", "/config"]
WORKDIR /app
ENTRYPOINT ["/usr/local/bin/python", "-m", "ops_bot.main"]
