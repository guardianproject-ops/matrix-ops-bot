POETRY ?= poetry run
SRC := ops_bot
TESTS := tests
SHELL := $(shell which bash)
APP_VERSION := $(shell git rev-parse --short HEAD)
DOCKER ?= docker

docker-build:
	DOCKER_BUILDKIT=1 $(DOCKER) build -f Dockerfile \
		--build-arg=$(APP_VERSION) \
		-t matrix-ops-bot:latest \
		.
fmt:
	$(POETRY) black $(SRC)
	$(POETRY) black $(TESTS)
	$(POETRY) isort --profile black $(SRC)
	$(POETRY) isort --profile black $(TESTS)

lint:
	$(POETRY) flake8 $(SRC)
	$(POETRY) bandit --silent --recursive $(SRC)
types:
	$(POETRY) mypy $(SRC)

test:
	$(POETRY) pytest $(TESTS)

freeze:
	poetry export --without dev  --format=requirements.txt --output requirements.frozen.txt

check:
	$(MAKE) fmt
	$(MAKE) lint
	$(MAKE) types
	$(MAKE) test
